# Use Jupyter Widgets as GUI for LaTeX to typeset a letter

[![Binder](https://mybinder.org/badge.svg)](https://mybinder.org/v2/gl/fkohrt%2FRMarkdown-sandbox/main?urlpath=git-pull?repo=https://gitlab.com/fkohrt/letter.git)

# Modifying the template

- edit `~/letter.git/pandoc-letter-din5008/letter.latex` as needed
- then `!cp ./pandoc-letter-din5008/letter.latex ~/.pandoc/templates` inside Jupyter Notebook

## Different font

### TeX Gyre Pagella

Add the following to change the main font to [TeX Gyre Pagella](https://tug.org/FontCatalogue/texgyrepagella/) ([more](http://www.gust.org.pl/projects/e-foundry/tex-gyre) [info](https://tex.stackexchange.com/questions/107877/why-did-the-gyre-project-fork-the-urw-fonts-rather-than-just-extending-them)):


```
\linespread{1.05}
\setmainfont[Mapping=tex-text,Ligatures=TeX,Numbers=OldStyle]{TeXGyrePagellaX-Regular}
```

Use the `luaotfload-tool` to discover font names: `!luaotfload-tool --find="TeXGyrePagella" --fuzzy`.

### FPL Neu

Dowload the FPL Neu `otf` files from [here](https://github.com/rstub/fplneu/releases) and add the following to change the main font to [FPL Neu](https://github.com/rstub/fplneu):

```
\setmainfont{fp9}[%
  Extension = .otf ,
  Path= /home/jovyan/letter.git/pandoc-letter-din5008/ ,
  UprightFont=*r8a,
  ItalicFont=*ri8a,
  BoldFont=*b8a,
  BoldItalicFont=*bi8a,
]
```